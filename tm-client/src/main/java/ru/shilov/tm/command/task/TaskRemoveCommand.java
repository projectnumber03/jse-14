package ru.shilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.TaskDTO;
import ru.shilov.tm.command.AbstractTerminalCommand;

import java.util.List;

public final class TaskRemoveCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String token = getToken();
        System.out.println("ВВЕДИТЕ ID ЗАДАЧИ:");
        @NotNull final String taskNumber = getServiceLocator().getTerminalService().nextLine();
        if (!numberCheck(taskNumber)) throw new Exception("Неверный ID задачи");
        @NotNull final List<TaskDTO> tasks = getEndPointLocator().getTaskEndPoint().findTasksByUserId(token);
        @NotNull final TaskDTO t = tasks.get(Integer.parseInt(taskNumber) - 1);
        getEndPointLocator().getTaskEndPoint().removeOneTaskByUserId(token, t.getId());
        System.out.println("[ЗАДАЧА УДАЛЕНА]");
    }

    @NotNull
    @Override
    public String getName() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удаление задачи";
    }

}
