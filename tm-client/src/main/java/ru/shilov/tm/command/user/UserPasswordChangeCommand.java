package ru.shilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.UserDTO;
import ru.shilov.tm.command.AbstractTerminalCommand;

public final class UserPasswordChangeCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String token = getToken();
        @NotNull final UserDTO u = getEndPointLocator().getUserEndPoint().findOneUser(token);
        System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
        u.setPassword(getServiceLocator().getTerminalService().nextLine());
        getEndPointLocator().getUserEndPoint().mergeUser(token, u);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "password-change";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Изменение пароля";
    }

}
