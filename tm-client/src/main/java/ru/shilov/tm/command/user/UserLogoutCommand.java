package ru.shilov.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.Exception_Exception;
import ru.shilov.tm.api.endpoint.Session;
import ru.shilov.tm.command.AbstractTerminalCommand;
import org.jetbrains.annotations.NotNull;

public final class UserLogoutCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @Nullable final String token = getSessionLocator().getToken();
        getEndPointLocator().getSessionEndPoint().removeSession(token);
        getSessionLocator().setToken(null);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "logout";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Выход из системы";
    }

}
