package ru.shilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.ProjectDTO;
import ru.shilov.tm.command.AbstractTerminalCommand;

import java.util.List;

public final class ProjectRemoveCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String token = getToken();
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        @NotNull final String projectNumber = getServiceLocator().getTerminalService().nextLine();
        if (!numberCheck(projectNumber)) throw new Exception("Неверный ID проекта");
        @NotNull final List<ProjectDTO> projects = getEndPointLocator().getProjectEndPoint().findProjectsByUserId(token);
        @NotNull final ProjectDTO p = projects.get(Integer.parseInt(projectNumber) - 1);
        getEndPointLocator().getProjectEndPoint().removeOneProjectByUserId(token, p.getId());
        System.out.println("[ПРОЕКТ УДАЛЕН]");
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удаление проекта";
    }

}
