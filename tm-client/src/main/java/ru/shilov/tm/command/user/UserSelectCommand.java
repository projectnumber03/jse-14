package ru.shilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.UserDTO;
import ru.shilov.tm.command.AbstractTerminalCommand;

import java.util.List;

public final class UserSelectCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String token = getToken();
        System.out.println("ВВЕДИТЕ ID ПОЛЬЗОВАТЕЛЯ:");
        @NotNull final String userNumber = getServiceLocator().getTerminalService().nextLine();
        if (!numberCheck(userNumber)) throw new Exception("Неверный ID пользователя");
        @NotNull final List<UserDTO> users = getEndPointLocator().getUserEndPoint().findAllUsers(token);
        @NotNull final UserDTO u = users.get(Integer.parseInt(userNumber) - 1);
        getServiceLocator().getTerminalService().printUserProperties(u);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String getName() {
        return "user-select";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Свойства пользователя";
    }

}
