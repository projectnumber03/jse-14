package ru.shilov.tm.api.context;

import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.Session;

public interface ISessionLocator {

    @Nullable
    String getToken();

    void setToken(@Nullable final String token);

}
