package ru.shilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.*;

import java.util.List;

public interface ITerminalService {

    String nextLine();

    void printAllProjects(@NotNull final List<ProjectDTO> projects);

    void printAllTasks(@NotNull final List<TaskDTO> tasks);

    void printAllUsers(@NotNull final List<UserDTO> users);

    void printProjectProperties(@NotNull final ProjectDTO p);

    void printTaskProperties(@NotNull final TaskDTO t);

    void printUserProperties(@NotNull final UserDTO u);

}
