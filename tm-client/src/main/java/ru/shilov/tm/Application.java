package ru.shilov.tm;

import ru.shilov.tm.context.Bootstrap;

public class Application {

    public static void main(String[] args) {
        new Bootstrap().init();
    }

}
