# Task Manager
## external repository
[jse-14](https://gitlab.com/projectnumber03/jse-14/)
## software
+ JRE
+ Java 8
+ Maven 4.0
## developer
Dmitry Shilov

email: [dunderflute@yandex.ru](mailto:dunderflute@yandex.ru)
## build application
```bash
mvn clean install
```
## run client
```bash
java -jar taskmanager/bin/tm-client.jar
```
## run server
```bash
java -jar taskmanager/bin/tm-server.jar
```