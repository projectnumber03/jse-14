package ru.shilov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.dto.SessionDTO;
import ru.shilov.tm.util.LocalDateTimeAdapter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity(name = "app_session")
@NoArgsConstructor
public final class Session extends AbstractEntity {

    @Nullable
    @ManyToOne
    @JsonIgnore
    private User user;

    @Nullable
    @Enumerated(EnumType.STRING)
    private User.Role role;

    @NotNull
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime creationDate = LocalDateTime.now();

    @Nullable
    private String signature;

    @Nullable
    public static SessionDTO toSessionDTO(@Nullable final Session session) {
        if (session == null) return null;
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setId(session.getId());
        if (session.getUser() != null) sessionDTO.setUserId(session.getUser().getId());
        sessionDTO.setRole(session.getRole());
        return sessionDTO;
    }

}
