package ru.shilov.tm.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.dto.TaskDTO;
import ru.shilov.tm.enumerated.Status;
import ru.shilov.tm.util.LocalDateAdapter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
@Entity(name = "app_task")
@NoArgsConstructor
public final class Task extends AbstractEntity {

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate start;

    @Nullable
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate finish;

    @Nullable
    @ManyToOne
    private Project project;

    @Nullable
    @ManyToOne
    private User user;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    public static TaskDTO toTaskDTO(@NotNull final Task task) {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setUserId(task.getUser().getId());
        if(task.getProject() != null) {
            taskDTO.setProjectId(task.getProject().getId());
        }
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        if (task.getStart() != null) taskDTO.setStart(DateTimeFormatter.ISO_LOCAL_DATE.format(task.getStart()));
        if (task.getFinish() != null) taskDTO.setFinish(DateTimeFormatter.ISO_LOCAL_DATE.format(task.getFinish()));
        taskDTO.setStatus(task.getStatus());
        return taskDTO;
    }

}
