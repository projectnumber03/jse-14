package ru.shilov.tm.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.ISessionEndPoint;
import ru.shilov.tm.api.service.ISessionService;
import ru.shilov.tm.context.Bootstrap;
import ru.shilov.tm.dto.SessionDTO;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.util.AES;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.shilov.tm.api.endpoint.ISessionEndPoint")
@NoArgsConstructor
@AllArgsConstructor
public final class SessionEndPointImpl implements ISessionEndPoint {

    @NotNull
    private Bootstrap bootstrap;

    @NotNull
    @WebMethod
    public String createSession(@NotNull final String login, @NotNull final String password) throws Exception {
        @Nullable final Session session = bootstrap.getSessionService().create(login, password);
        @Nullable final SessionDTO sessionDTO = Session.toSessionDTO(session);
        @NotNull final String key = bootstrap.getSettingService().getProperty("decryption.key");
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        @NotNull final String json = objectMapper.writeValueAsString(sessionDTO);
        return AES.encrypt(json, key);
    }

    @Override
    @WebMethod
    public void removeSession(@Nullable final String token) throws Exception {
        @NotNull final Session session = bootstrap.getSessionService().decrypt(token);
        bootstrap.getSessionService().removeByUserId(session.getUser().getId());
    }

}
