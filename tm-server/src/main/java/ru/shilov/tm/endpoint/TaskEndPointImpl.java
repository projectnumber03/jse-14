package ru.shilov.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.endpoint.ITaskEndPoint;
import ru.shilov.tm.api.service.ISessionService;
import ru.shilov.tm.api.service.ITaskService;
import ru.shilov.tm.context.Bootstrap;
import ru.shilov.tm.dto.TaskDTO;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.entity.User;

import javax.jws.WebService;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@WebService(endpointInterface = "ru.shilov.tm.api.endpoint.ITaskEndPoint")
@NoArgsConstructor
@AllArgsConstructor
public final class TaskEndPointImpl implements ITaskEndPoint {

    @NotNull
    private Bootstrap bootstrap;

    @NotNull
    @Override
    public List<TaskDTO> findAllTasks(@Nullable final String token) throws Exception {
        bootstrap.getSessionService().validate(token);
        return bootstrap.getTaskService().findAll().stream().map(Task::toTaskDTO).collect(Collectors.toList());
    }

    @Override
    public @NotNull List<TaskDTO> findAllTasksOrderByFinish(@Nullable final String token) throws Exception {
        return bootstrap.getTaskService()
                .findByUserIdOrderBy(bootstrap.getSessionService().validate(token).getUser().getId(), "finish")
                .stream().map(Task::toTaskDTO).collect(Collectors.toList());
    }

    @Override
    public @NotNull List<TaskDTO> findAllTasksOrderByStart(@Nullable final String token) throws Exception {
        return bootstrap.getTaskService()
                .findByUserIdOrderBy(bootstrap.getSessionService().validate(token).getUser().getId(), "start")
                .stream().map(Task::toTaskDTO).collect(Collectors.toList());
    }

    @Override
    public @NotNull List<TaskDTO> findAllTasksOrderByStatus(@Nullable final String token) throws Exception {
        return bootstrap.getTaskService()
                .findByUserIdOrderBy(bootstrap.getSessionService().validate(token).getUser().getId(), "status")
                .stream().map(Task::toTaskDTO).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public TaskDTO findOneTask(@Nullable final String token, @NotNull final String id) throws Exception {
        bootstrap.getSessionService().validate(token);
        return Task.toTaskDTO(bootstrap.getTaskService().findOne(id));
    }

    @NotNull
    @Override
    public List<TaskDTO> findTasksByUserId(@Nullable final String token) throws Exception {
        return bootstrap.getTaskService().findByUserId(bootstrap.getSessionService().validate(token).getUser().getId()).stream().map(Task::toTaskDTO).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<TaskDTO> findTasksByProjectId(@Nullable final String token, @NotNull final String id) throws Exception {
        return bootstrap.getTaskService().findByProjectId(bootstrap.getSessionService().validate(token).getUser().getId(), id).stream().map(Task::toTaskDTO).collect(Collectors.toList());
    }

    @Override
    public void removeAllTasks(@Nullable final String token) throws Exception {
        bootstrap.getSessionService().validate(token, Arrays.asList(User.Role.ADMIN));
        bootstrap.getTaskService().removeAll();
    }

    @Override
    public void removeTasksByUserId(@Nullable final String token) throws Exception {
        bootstrap.getTaskService().removeByUserId(bootstrap.getSessionService().validate(token).getUser().getId());
    }

    @Override
    public void removeOneTaskByUserId(@Nullable final String token, @Nullable final String id) throws Exception {
        bootstrap.getTaskService().removeOneByUserId(bootstrap.getSessionService().validate(token).getUser().getId(), id);
    }

    @NotNull
    @Override
    public TaskDTO persistTask(@Nullable final String token, @NotNull final TaskDTO t) throws Exception {
        @NotNull final Session currentSession = bootstrap.getSessionService().validate(token);
        t.setUserId(currentSession.getUser().getId());
        bootstrap.getTaskService().persist(TaskDTO.toTask(bootstrap, t));
        return t;
    }

    @NotNull
    @Override
    public TaskDTO mergeTask(@Nullable final String token, @NotNull final TaskDTO t) throws Exception {
        bootstrap.getSessionService().validate(token);
        bootstrap.getTaskService().merge(TaskDTO.toTask(bootstrap, t));
        return t;
    }

    @NotNull
    @Override
    public List<TaskDTO> findTasksByNameOrDescription(@Nullable final String token, @NotNull final String value) throws Exception {
        return bootstrap.getTaskService()
                .findByNameOrDescription(bootstrap.getSessionService().validate(token).getUser().getId(), value)
                .stream().map(Task::toTaskDTO).collect(Collectors.toList());
    }

}
