package ru.shilov.tm.error;

public final class EntityPersistException extends RuntimeException {

    public EntityPersistException() {
        super("Ошибка добавления объекта");
    }

    public EntityPersistException(Throwable throwable) {
        super("Ошибка добавления объекта", throwable);
    }

}
