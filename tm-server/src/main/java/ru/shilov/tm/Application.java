package ru.shilov.tm;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.context.Bootstrap;

public final class Application {

    public static void main(@NotNull final String[] args) throws Exception {
        new Bootstrap().init();
    }

}
