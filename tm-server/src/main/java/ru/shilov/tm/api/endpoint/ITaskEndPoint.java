package ru.shilov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.dto.TaskDTO;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndPoint {

    @NotNull
    @WebMethod
    List<TaskDTO> findAllTasks(@Nullable final String token) throws Exception;

    @NotNull
    @WebMethod
    List<TaskDTO> findAllTasksOrderByFinish(@Nullable final String token) throws Exception;

    @NotNull
    @WebMethod
    List<TaskDTO> findAllTasksOrderByStart(@Nullable final String token) throws Exception;

    @NotNull
    @WebMethod
    List<TaskDTO> findAllTasksOrderByStatus(@Nullable final String token) throws Exception;

    @NotNull
    @WebMethod
    TaskDTO findOneTask(@Nullable final String token, @NotNull final String id) throws Exception;

    @NotNull
    @WebMethod
    List<TaskDTO> findTasksByUserId(@Nullable final String token) throws Exception;

    @NotNull
    @WebMethod
    List<TaskDTO> findTasksByProjectId(@Nullable final String token, @NotNull final String id) throws Exception;

    @WebMethod
    void removeAllTasks(@Nullable final String token) throws Exception;

    @WebMethod
    void removeTasksByUserId(@Nullable final String token) throws Exception;

    @WebMethod
    void removeOneTaskByUserId(@Nullable final String token, @Nullable final String id) throws Exception;

    @Nullable
    @WebMethod
    TaskDTO persistTask(@Nullable final String token, @NotNull final TaskDTO t) throws Exception;

    @Nullable
    @WebMethod
    TaskDTO mergeTask(@Nullable final String token, @NotNull final TaskDTO t) throws Exception;

    @NotNull
    List<TaskDTO> findTasksByNameOrDescription(@Nullable final String token, @NotNull final String value) throws Exception;

}
