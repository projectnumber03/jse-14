package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Session;

import java.sql.SQLException;
import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @Nullable
    Session findOneByUserId(@NotNull final String userId, @NotNull final String id) throws SQLException;

    void removeByUserId(@NotNull final String userId) throws SQLException;

    @Override
    @NotNull List<Session> findAll();

    @Nullable
    @Override
    Session findOne(@NotNull final String id);

    @Override
    void removeAll();

    @Override
    void persist(@NotNull final Session entity);

}
