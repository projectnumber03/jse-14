package ru.shilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.error.NumberToIdTransformException;

import java.util.List;

public interface ITaskService extends IService<Task> {

    @NotNull
    List<Task> findByUserId(@Nullable final String userId) throws NoSuchEntityException;

    void removeByUserId(@Nullable final String userId) throws EntityRemoveException;

    @NotNull
    List<Task> findByProjectId(@Nullable final String userId, @Nullable final String projectId) throws NoSuchEntityException;

    @NotNull
    List<Task> findByNameOrDescription(@Nullable final String userId, @Nullable final String value) throws NoSuchEntityException;

    void removeOneByUserId(@Nullable final String userId, @Nullable final String id) throws Exception;

    List<Task> findByUserIdOrderBy(@NotNull final String userId, @NotNull final String field);

}
