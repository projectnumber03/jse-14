package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    void removeOneById(@NotNull final String id) throws SQLException;

    @Nullable
    User findByLogin(@NotNull final String login) throws SQLException;

    @Override
    @NotNull List<User> findAll();

    @Nullable
    @Override
    User findOne(@NotNull final String id);

    @Override
    void removeAll();

    @Override
    void persist(@NotNull final User entity);

    @Override
    void merge(@NotNull final User entity);

}
