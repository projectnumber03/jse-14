package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.List;

public interface IRepository<T> {

    @NotNull
    List<T> findAll();

    @Nullable
    T findOne(@NotNull final String id);

    void removeAll();

    void persist(@NotNull final T entity);

    void merge(@NotNull final T entity);

}
