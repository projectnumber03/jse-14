package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Task;

import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @NotNull
    List<Task> findByProjectId(@NotNull final String userId, @NotNull final String projectId) throws SQLException;

    @NotNull
    List<Task> findByNameOrDescription(@NotNull final String userId, @NotNull final String value) throws SQLException;

    void removeByUserId(@NotNull final String userId) throws SQLException;

    @NotNull
    List<Task> findByUserId(@NotNull final String userId) throws SQLException;

    void removeOneByUserId(@NotNull final String userId, @NotNull final String id) throws SQLException;

    @NotNull
    @Override
    List<Task> findAll();

    @Nullable
    @Override
    Task findOne(@NotNull final String id);

    @Override
    void removeAll();

    @Override
    void persist(@NotNull final Task entity);

    @Override
    void merge(@NotNull final Task entity);

    List<Task> findByUserIdOrderBy(@NotNull final String userId, @NotNull final String field);

}
