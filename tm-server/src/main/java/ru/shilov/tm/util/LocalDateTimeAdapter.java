package ru.shilov.tm.util;

import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class LocalDateTimeAdapter extends XmlAdapter<String, LocalDateTime> {

    @NotNull
    @Override
    public LocalDateTime unmarshal(@NotNull final String s) {
        return LocalDateTime.parse(s, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    @NotNull
    @Override
    public String marshal(@NotNull final LocalDateTime localDate) {
        return localDate.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

}
