package ru.shilov.tm.context;

import lombok.Getter;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.endpoint.*;
import ru.shilov.tm.api.repository.ISettingRepository;
import ru.shilov.tm.api.service.*;
import ru.shilov.tm.endpoint.*;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.EntityPersistException;
import ru.shilov.tm.error.InitializationException;
import ru.shilov.tm.repository.SettingRepositoryImpl;
import ru.shilov.tm.service.*;
import ru.shilov.tm.util.SignatureUtil;

import javax.persistence.EntityManagerFactory;
import javax.xml.ws.Endpoint;
import java.util.Scanner;

public final class Bootstrap {

    @NotNull
    private final ISettingRepository settingRepo = new SettingRepositoryImpl();

    @Getter
    @NotNull
    private final ISettingService settingService = new SettingServiceImpl(settingRepo);

    @Getter
    @NotNull
    private final IProjectService projectService = ProjectServiceImpl.builder().bootstrap(this).build();

    @Getter
    @NotNull
    private final ITaskService taskService = TaskServiceImpl.builder().bootstrap(this).build();

    @Getter
    @NotNull
    private final IUserService userService = UserServiceImpl.builder().bootstrap(this).build();

    @Getter
    @NotNull
    private final ISessionService sessionService = SessionServiceImpl.builder().bootstrap(this).build();

    @NotNull
    private final IProjectEndPoint projectEndpoint = new ProjectEndPointImpl(this);

    @NotNull
    private final ITaskEndPoint taskEndpoint = new TaskEndPointImpl(this);

    @NotNull
    private final IUserEndPoint userEndpoint = new UserEndPointImpl(this);

    @NotNull
    private final ISessionEndPoint sessionEndpoint = new SessionEndPointImpl(this);

    @Getter
    @NotNull
    private final EntityManagerFactory entityManagerFactory = initEntityManagerFactory();

    public void init() throws Exception {
        initEntities();
        @NotNull final String hostName = settingService.getProperty("hostname");
        @NotNull final String port = settingService.getProperty("port");
        Endpoint.publish(String.format("http://%s:%s/projectservice?wsdl", hostName, port), projectEndpoint);
        Endpoint.publish(String.format("http://%s:%s/taskservice?wsdl", hostName, port), taskEndpoint);
        Endpoint.publish(String.format("http://%s:%s/userservice?wsdl", hostName, port), userEndpoint);
        Endpoint.publish(String.format("http://%s:%s/sessionservice?wsdl", hostName, port), sessionEndpoint);
        @NotNull final Scanner scanner = new Scanner(System.in);
        while (!scanner.nextLine().equals("exit")) { }
        System.exit(0);
    }

    private void initEntities() throws Exception {
        try {
            @NotNull final String salt = settingService.getProperty("salt");
            @NotNull final String cycle = settingService.getProperty("cycle");
            taskService.removeAll();
            projectService.removeAll();
            sessionService.removeAll();
            userService.removeAll();
            userService.persist(new User("user", SignatureUtil.sign("123", salt, Integer.parseInt(cycle)), User.Role.USER));
            userService.persist(new User("admin", SignatureUtil.sign("123", salt, Integer.parseInt(cycle)), User.Role.ADMIN));
        } catch (EntityPersistException e) {
            throw new InitializationException();
        }
    }

    private EntityManagerFactory initEntityManagerFactory() {
        Configuration configuration = new Configuration().configure();
        configuration.addAnnotatedClass(User.class);
        configuration.addAnnotatedClass(Project.class);
        configuration.addAnnotatedClass(Task.class);
        configuration.addAnnotatedClass(Session.class);
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        return configuration.buildSessionFactory(builder.build());
    }

}
