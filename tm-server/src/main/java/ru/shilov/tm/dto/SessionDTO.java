package ru.shilov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.context.Bootstrap;
import ru.shilov.tm.entity.Session;
import ru.shilov.tm.entity.User;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
@NoArgsConstructor
public final class SessionDTO extends AbstractEntityDTO {

    @Nullable
    private String userId;

    @Nullable
    private User.Role role;

    @Nullable
    private String creationDate;

    @Nullable
    private String signature;

    @NotNull
    public static Session toSession(@NotNull final Bootstrap bootstrap, @NotNull final SessionDTO sessionDTO) throws Exception {
        @NotNull final Session session = new Session();
        session.setId(sessionDTO.getId());
        session.setUser(bootstrap.getUserService().findOne(sessionDTO.getUserId()));
        session.setRole(sessionDTO.getRole());
        if (sessionDTO.getCreationDate() != null) {
            session.setCreationDate(LocalDateTime.parse(sessionDTO.getCreationDate(), DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        }
        return session;
    }

}
