package ru.shilov.tm.repository;

import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.repository.IProjectRepository;
import ru.shilov.tm.entity.Project;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;

@SuperBuilder
public class ProjectRepositoryImpl extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public List<Project> findByNameOrDescription(@NotNull final String userId, @NotNull final String value) {
        @NotNull final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Project> criteria = builder.createQuery(getEntityClass());
        @NotNull final Root<Project> root = criteria.from(getEntityClass());
        @NotNull final Predicate namePredicate = builder.like(root.get("name"), String.format("%%%s%%", value));
        @NotNull final Predicate descriptionPredicate = builder.like(root.get("description"), String.format("%%%s%%", value));
        criteria.select(root).where(builder.or(namePredicate, descriptionPredicate));
        @NotNull final TypedQuery<Project> typed = entityManager.createQuery(criteria);
        return typed.getResultList();
    }

    @Override
    public void removeByUserId(@NotNull final String userId) {
        findByUserId(userId).forEach(entityManager::remove);
        entityManager.flush();
    }

    @NotNull
    @Override
    public List<Project> findByUserId(@NotNull final String userId) {
        @NotNull final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Project> criteria = builder.createQuery(getEntityClass());
        @NotNull final Root<Project> root = criteria.from(getEntityClass());
        criteria.select(root).where(builder.equal(root.get("user").get("id"), userId));
        @NotNull final TypedQuery<Project> typed = entityManager.createQuery(criteria);
        return typed.getResultList();
    }

    @Override
    public void removeOneByUserId(@NotNull final String userId, @NotNull final String id) {
        @NotNull final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaDelete<Project> criteria = builder.createCriteriaDelete(getEntityClass());
        @NotNull final Root<Project> root = criteria.from(getEntityClass());
        @NotNull final Predicate idPredicate = builder.equal(root.get("id"), id);
        @NotNull final Predicate userIdPredicate = builder.equal(root.get("user").get("id"), userId);
        criteria.where(builder.and(idPredicate, userIdPredicate));
        entityManager.createQuery(criteria).executeUpdate();
        entityManager.flush();
    }

    @NotNull
    @Override
    public List<Project> findByUserIdOrderBy(@NotNull final String userId, @NotNull final String field) {
        @NotNull final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Project> criteria = builder.createQuery(getEntityClass());
        @NotNull final Root<Project> root = criteria.from(getEntityClass());
        criteria.select(root).where(builder.equal(root.get("user").get("id"), userId)).orderBy(builder.asc(root.get(field)));
        @NotNull final TypedQuery<Project> typed = entityManager.createQuery(criteria);
        return typed.getResultList();
    }


    @Override
    public Class<Project> getEntityClass() {
        return Project.class;
    }

}
