package ru.shilov.tm.repository;

import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.ISessionRepository;
import ru.shilov.tm.entity.Session;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;

@SuperBuilder
public class SessionRepositoryImpl extends AbstractRepository<Session> implements ISessionRepository {

    @Nullable
    @Override
    public Session findOneByUserId(@NotNull final String userId, @NotNull final String id) {
        @NotNull final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Session> criteria = builder.createQuery(Session.class);
        @NotNull final Root<Session> root = criteria.from(Session.class);
        @NotNull final Predicate idPredicate = builder.equal(root.get("id"), id);
        @NotNull final Predicate userIdPredicate = builder.equal(root.get("user").get("id"), userId);
        criteria.select(root).where(builder.and(userIdPredicate, idPredicate));
        @NotNull final TypedQuery<Session> typed = entityManager.createQuery(criteria);
        return typed.getSingleResult();
    }

    @Override
    public void removeByUserId(@NotNull final String userId) {
        @NotNull final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaDelete<Session> criteria = builder.createCriteriaDelete(getEntityClass());
        @NotNull final Root<Session> root = criteria.from(getEntityClass());
        criteria.where(builder.equal(root.get("user").get("id"), userId));
        entityManager.createQuery(criteria).executeUpdate();
        entityManager.flush();
    }

    @Override
    public Class<Session> getEntityClass() {
        return Session.class;
    }

}
