package ru.shilov.tm.repository;

import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@SuperBuilder
public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    @NotNull
    protected EntityManager entityManager;

    public abstract Class<T> getEntityClass();

    @NotNull
    @Override
    public List<T> findAll() {
        @NotNull final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<T> criteriaQuery = builder.createQuery(getEntityClass());
        @NotNull final Root<T> root = criteriaQuery.from(getEntityClass());
        criteriaQuery.select(root);
        @NotNull final TypedQuery<T> typedQuery = entityManager.createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    @Nullable
    @Override
    public T findOne(@NotNull final String id) {
        return entityManager.find(getEntityClass(), id);
    }

    @Override
    public void removeAll() {
        findAll().forEach(entityManager::remove);
        entityManager.flush();
    }

    @Override
    public void persist(@NotNull final T entity) {
        entityManager.persist(entity);
        entityManager.flush();
    }

    @Override
    public void merge(@NotNull final T entity) {
        entityManager.merge(entity);
        entityManager.flush();
    }

}
