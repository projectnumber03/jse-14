package ru.shilov.tm.repository;

import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.IUserRepository;
import ru.shilov.tm.entity.User;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@SuperBuilder
public class UserRepositoryImpl extends AbstractRepository<User> implements IUserRepository {

    @Override
    public void removeOneById(@NotNull final String id) {
        entityManager.remove(findOne(id));
        entityManager.flush();
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull final CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<User> criteria = builder.createQuery(getEntityClass());
        @NotNull final Root<User> root = criteria.from(getEntityClass());
        criteria.select(root).where(builder.equal(root.get("login"), login));
        @NotNull final TypedQuery<User> typed = entityManager.createQuery(criteria);
        return typed.getSingleResult();
    }

    @Override
    public Class<User> getEntityClass() {
        return User.class;
    }

}
