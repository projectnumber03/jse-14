import org.junit.Ignore;
import org.junit.Test;
import ru.shilov.tm.api.service.IProjectService;
import ru.shilov.tm.api.service.ISessionService;
import ru.shilov.tm.api.service.ITaskService;
import ru.shilov.tm.api.service.IUserService;
import ru.shilov.tm.context.Bootstrap;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.repository.ProjectRepositoryImpl;
import ru.shilov.tm.service.ProjectServiceImpl;
import ru.shilov.tm.service.SessionServiceImpl;
import ru.shilov.tm.service.TaskServiceImpl;
import ru.shilov.tm.service.UserServiceImpl;

import javax.persistence.EntityManager;
import java.time.LocalDate;

public class HibernateTest {

    @Ignore
    @Test
    public void findAllTest() {
        Bootstrap b = new Bootstrap();
        IUserService userService = UserServiceImpl.builder().bootstrap(b).build();
        System.out.println(userService.findAll());
    }

    @Ignore
    @Test
    public void findOneTest() throws Exception {
        Bootstrap b = new Bootstrap();
        IUserService userService = UserServiceImpl.builder().bootstrap(b).build();
        System.out.println(userService.findOne("4a4d242e-b1ed-448f-80f4-9828887009b9"));
    }

    @Ignore
    @Test
    public void findByLoginTest() {
        Bootstrap b = new Bootstrap();
        IUserService userService = UserServiceImpl.builder().bootstrap(b).build();
        System.out.println(userService.findByLogin("admin"));
    }

    @Ignore
    @Test
    public void test() throws Exception {
        Bootstrap b = new Bootstrap();
        IProjectService projectService = ProjectServiceImpl.builder().bootstrap(b).build();
        ITaskService taskService = TaskServiceImpl.builder().bootstrap(b).build();
        IUserService userService = UserServiceImpl.builder().bootstrap(b).build();
        Project p = new Project();
        p.setUser(userService.findByLogin("user"));
        p.setName("test-project4");
        p.setDescription("test-description");
        p.setStart(LocalDate.now());
        p.setFinish(LocalDate.now().plusDays(2));
        projectService.persist(p);
        Task t = new Task();
        t.setUser(userService.findByLogin("user"));
        t.setName("test-task1");
        t.setDescription("test-description");
        t.setStart(LocalDate.now());
        t.setFinish(LocalDate.now().plusDays(1));
        t.setProject(p);
        taskService.persist(t);
        t = new Task();
        t.setUser(userService.findByLogin("user"));
        t.setName("test-task2");
        t.setDescription("test-description");
        t.setStart(LocalDate.now());
        t.setFinish(LocalDate.now().plusDays(1));
        t.setProject(p);
        taskService.persist(t);
    }

    @Ignore
    @Test
    public void test2() throws Exception {
        Bootstrap b = new Bootstrap();
        IUserService userService = UserServiceImpl.builder().bootstrap(b).build();
        ISessionService sessionService = SessionServiceImpl.builder().bootstrap(b).build();
        IProjectService projectService = ProjectServiceImpl.builder().bootstrap(b).build();
        User user = userService.findByLogin("admin");
        Project p = new Project();
        p.setName("test-project");
        p.setDescription("test-description");
        p.setStart(LocalDate.now());
        p.setFinish(LocalDate.now().plusDays(1));
    }

    @Ignore
    @Test
    public void test3() {
        Bootstrap b = new Bootstrap();
        EntityManager em = b.getEntityManagerFactory().createEntityManager();
//        List<Task> tasks = TaskRepositoryImpl.builder().entityManager(em).build().findByProjectId("87450cc8-4f27-439b-833b-d71d3bdd18bc", "7f421ee3-b0a7-40ec-b023-cf82440e030f");
        em.getTransaction().begin();
//        tasks.forEach(em::remove);
        ProjectRepositoryImpl.builder().entityManager(em).build().removeOneByUserId("25dccb9b-927f-4278-9592-d75bb5edfcf7", "3073a2e2-10e5-4142-9b24-8e4d8c7b54aa");
        em.flush();
        em.getTransaction().commit();
        em.close();
    }

    @Ignore
    @Test
    public void removeTest() throws Exception {
        Bootstrap b = new Bootstrap();
        IUserService userService = UserServiceImpl.builder().bootstrap(b).build();
        userService.persist(new User("test", "123", User.Role.USER));
        System.out.println(userService.findAll());
        userService.removeOneById(userService.findByLogin("test").getId());
        System.out.println(userService.findAll());
    }

}
